<!--
SPDX-FileCopyrightText: 2024 Helmholtz Centre for Environmental Research GmbH - UFZ

SPDX-License-Identifier: LicenseRef-UFZ-GPL-3.0
-->

# Description of defendeR

DefendeR is a R script snippet that provides functionality for data formatting, processing, filtering,
validation, and visualization of multiple assignments (MultiAs) in ultra-high
resolution mass spectrometry (UHRMS) data from natural organic matter (NOM). The concept, workflow
description are provided in the main text in journal publication (DOI: 10.1021/acs.analchem.4c00489).

## Description of main folders / files

The entire defendeR snippet consists of 4 folders and 4 other configuration files.
The main R scripts are stored in folder **"scripts"**, example data used in the manuscript
can be found in folder **"data"**, the license for this snippet can be
found in folder **"license"**, and configurations to run this snippet are stored in the
folder **"config"**. Before using the R code with the user's own data and pipeline,
please refer to this **README.md** file carefully.

### Folder: scripts

This folder contains the main R script files as an R Markdown
(.rmd) file that users can us directly in
Rstudio to reproduce results shown in the vignette.

- **defendeR.rmd**: The R Markdown file for extracting MultiAs from initial unfiltered molecular
formula (MF) list and inspecting the mass error distribution of homologous
series in mDa scale.
- **defendeR.nb.html**: Vignette, a html file knitted from the .rmd file described above.
- **defendeR.Rproj**: R project environment in Rstudio.
- **defendeR.Rproj.license**: License to R project environment mentioned above.

### Folder: data

This folder contains raw data in a comma-separated value
(.csv) files that can be used with the snippet. These are the same data as described in
the accompanying manuscript (Gao et al. AnalChem, 2024).

- **SRFA_formuals_CFC_N5S3.csv**: Suwannee River Fulvic Acid (SRFA Ⅱ, 2S101H,
Standard from the International Humic Substances Society) represents a complex
organic matter mixture with elements and isotopes at their natural abundance
and was measured with DI (SRFA dataset). Chemical formula configurations (CFC)
were set as: (C: 1-80, H: 1-198, O: 0-40, **N: 0-5**, **S: 0-3**), O/C (0-1.2),
H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **SRFA_formuals_CFC_N3S1.csv**: Suwannee River Fulvic Acid (SRFA Ⅱ, 2S101H,
Standard from the International Humic Substances Society) represents a complex
organic matter mixture with elements and isotopes at their natural abundance
and was measured with DI (SRFA dataset). Chemical formula configurations (CFC)
were set as: (C: 1-80, H: 1-198, O: 0-40, **N: 0-3**, **S: 0-1**), O/C (0-1.2),
H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **SRFA_Na_adducts.csv**: Suwannee River Fulvic Acid (SRFA Ⅱ, 2S101H, Standard
from the International Humic Substances Society) represents a complex organic
matter mixture with elements and isotopes at their natural abundance and was
measured with DI (SRFA dataset). Chemical formula configurations (CFC) were set
as: (C: 1-80, H: 1-198, O: 0-40, **N: 0-3**, **S: 0-1**, **Na: 0-1**), O/C
(0-1.2), H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **SRFA_CBZ_2H.csv**: SRFA was photo-irradiated together with carbamazepine-
D10 (CBZ-D10, Isotopic purity >95%, Toronto Research Chemicals, Toronto,
CA) - a process, which has been shown to form covalent bonds between NOM
molecules and carbamazepine introducing D into NOM-MFs.  Chemical formula
configurations (CFC) were set as: (C: 1-80, H: 1-198, O: 0-40, **N: 0-3**,
**S: 0-1**, **D: 0-5**), O/C (0-1.2), H/C (0.3-3), N/C (0-1.5), DBE (0-25),
and DBE-O (-10-10).
- **EfOM_Oz_18O.csv**: Previously published LC-FT-ICR MS data from effluent
organic matter (EfOM, from a wastewater treatment plant effluent) ozonated with
heavy ozone (<sup>18</sup>O<sub>3</sub>). Chemical formula configurations (CFC)  
were set as: (C: 1-80, H: 1-198, **<sup>16</sup>O: 0-40, N: 0-4, S: 0-2, <sup>18</sup>O: 0-5**),  
O/C (0-1.2), H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **DW_Cl2.csv**: Previously published LC-FT-ICR MS data from drinking water
(DW) disinfected with chlorine, introducing <sup>35</sup>Cl and <sup>37</sup>Cl  
at their natural
abundance. Chemical formula configurations (CFC) were set as: (C: 1-80, H:
1-198, O: 0-40, **N: 0-2**, **S: 0-1**, **<sup>35</sup>Cl: 0-3**, **<sup>37</sup>Cl: 0-3**), O/C
(0-1.2), H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **CHOCl_CHOP.csv**: Previously published LC-FT-ICR MS data from drinking
water (DW) disinfected with chlorine, introducing **<sup>35</sup>Cl** and **<sup>37</sup>Cl**  
at their
natural abundance. Chemical formula configurations (CFC) were set as: (C: 1-80
, H: 1-198, O: 0-40, **N: 0-2**, **S: 0-1**, **<sup>35</sup>Cl: 0-3**, **<sup>37</sup>Cl: 0-3**,
**P:0-2**), O/C (0-1.2), H/C (0.3-3), N/C (0-1.5), DBE (0-25), and DBE-O (-10-10).
- **Repository_file_definition.csv**: Definitions of column names used in CSV
files.

## Tutorial

Before executing this R snippet, the user needs to install the suitable R
programming language version and  related R packages (see section **R
environment configuration setup**).
The snippet could be run within teh provided R project or separately
with markdown file and data provided.
If the user is familiar with R projects, he/she may start from **"Run the snippet within
R project"**, otherwise he/she may jump to **"Run the snippet without R project"**.

### R environment configurations setup

Before running the R codes provided, please ensure that R and R packages are installed
and loaded in proper version, i.e. similar or above.

- **R** version: 4.2.1
- R Package **knitr**: 1.45
- R package  **ggplot2**: 3.5.0
- R package **DT**: 0.32
- R package **htmltools**: 0.5.7
- R package **data.table**: 1.15.2
- R package **dplyr**: 1.1.4
- R package **stringr**: 1.5.1
- R package **tidyr**: 1.3.1  

### Run the snippet within R project

#### Source code download to local system

To get started, either download the source code as .zip file or clone the entire git project
with HTTPS to your local computers.

#### Create R project and load .Rmd file

Create R project and associate a project with an existing directory where the defendeR
is located, or open the defendR project by double-clicking the R project in the defendeR folder.
After loading/creating R project from defendeR, all folders and files could be seen in
the **Files** pane. Load the **defendeR.Rmd** file in the **scripts** folder.

#### Run the chunks in R project and .Rmd file

1. Import data: data used here are well-calibrated molecular formula lists. Note that the defeneR does
not involve molecular formula assignments, which should be done beforehand.
To embed this snippet to user's own pipeline, up-stream data prepared for MultiAs
exclusion should be **.csv** file (s) with at least teh following columns provided
(with columns named as): peak m/z values (numeric, > 5 digits, with colname as **peak_mz**),
formula mass (numeric, > 5 digits, with colname as **formula_mass**), formula class
(character, with colname as **formula_class**, indicating elements contained,
i.e. CHO, CHNO, CHNOS, CHOS), and relative error (numeric, > 5 digits, in ppm, name as **formula_error_relative**).

2. Selection of MultiAs: multiple assignments (MultiAs) are selected by judging
if one single peak m/z refers more than one molecular formula. If so, these molecular
formula shall be selected as MultiAs for false-assignment detection and exclusion.

3. Calculate the mass error in mDa and KMD values in CH2 scale.

4. Then median values are calculated within subgroups that share the same KMD value
and formula class. And the median values will be assigned to every member forming this subgroups.
Finally, every molecular formula will have one median value.

5. To filter the false-assignments, the median values of multiple assignments to a
single peak will be simple compared, the formula with median closer to zero will
then be determined as true-assignment. The formula with non-zero mean will be determined
as false-assignment and discarded.

6. Mass error distribution of all molecular formulas could be visualized before
and after data filtration for users needs.  

### Run the snippet without R project

#### Source code download to local systems

To get started, either download the source code as .zip file or clone the project
with HTTPS to your local computer.

#### Load ".Rmd" file and open ".nb.html" file

*Copy and paste the **"defendeR.rmd"** file to the folder **"data"**, then open the
.Rmd file in **"data"** in Rstudio.* You can also open the **"defendeR.nb.html"** file
in the web browser for a reference on the results of each chunk / syntax.

#### Run the chunks in .Rmd file and reproduce results shown in defendeR.nb.html

1. Import data: data used here are well-calibrated molecular formula lists. Note that the defeneR does
not involve molecular formula assignments, which should be done beforehand.
To embed this snippet to user's own pipeline, up-stream data prepared for MultiAs
exclusion should be **.csv** file (s) with at least teh following columns provided
(with columns named as): peak m/z values (numeric, > 5 digits, with colname as **peak_mz**),
formula mass (numeric, > 5 digits, with colname as **formula_mass**), formula class
(character, with colname as **formula_class**, indicating elements contained,
i.e. CHO, CHNO, CHNOS, CHOS), and relative error (numeric, > 5 digits, in ppm, name as **formula_error_relative**).

2. Selection of MultiAs: multiple assignments ( MultiAs) are selected by judging
if one single peak refers more than one molecular formula. If so, these molecular
formula shall be selected as MultiAs for false-assignment detection and exclusion.

3. Calculate the mass error in mDa and KMD values in CH2 scale.  

4. Then median values are calculated within subgroups that share the same KMD value
and formula class. And the median values will be assigned to every member forming this subgroups.
Finally, every molecular formula shall has one median value.

5. To filter the false-assignments, the median values of multiple assignments to a
single peak will be simple compared, the formula with median closer to zero will
then be determined as true-assignment. The formula with non-zero mean will be determined
as false-assignment and discarded.

6. Mass error distribution of all molecular formulas could be visualized before
and after data filtration for users needs.

## How to cite this material

Gao, S., Lechtenfeld, O., & Wurz, J. (2024). defendeR (v0.1.0).
Zenodo. <https://doi.org/10.5281/zenodo.10928196>

## Contact(s)

Should you have any conceptual questions, please contact Mr. Shuxian Gao
via Email: <shuxian.gao@ufz.de>
